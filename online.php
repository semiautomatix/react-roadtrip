/**
 * Tim Gaul
 * CRM integration
 * 2016-05-11
 */
// add the action 
// define the wpcf7_mail_sent callback 
function action_wpcf7_before_send_mail( $contact_form ) { 
    // make action magic happen here... 
    $submission = WPCF7_Submission::get_instance();
  
    if ( $submission ) {
    	$posted_data = $submission->get_posted_data();

		//$conn = oci_connect("roadtrip", "Pa55w0rd1", "//41.169.2.10/xe");
		$conn = oci_connect("roadtrip", "Pa55w0rd1", "//www.roadtrip.co.za/xe");
		if (!$conn) {
		   $m = oci_error();
		   error_log(print_r($m['message'], TRUE));
		}
		else {
		   error_log(print_r("Connected to Oracle!", TRUE));
		}
		
		// get the ID of the manufacturer
		$manufacturerId = null;
		
		$query = 'select id from rtp_manufacturer where UPPER(MANUFACTURER_DESCRIPTION) = :manufacturer'; //  OTHER	
		$stid = oci_parse($conn, $query);
		
		oci_bind_by_name($stid, ":manufacturer", $posted_data['VehicleMake']);		
		oci_execute($stid);

		$count = 0;
		while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
			$manufacturerId = $row['ID'];
			$count++;
		}
		
		if ($count == 0) {
			$query = 'select id from rtp_manufacturer where UPPER(MANUFACTURER_DESCRIPTION) = \'OTHER\''; //  OTHER	
			$stid = oci_parse($conn, $query);		
			oci_execute($stid);

			if (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
				$manufacturerId = $row['ID'];
			}
		}

		oci_free_statement($stid);
		
		// get the ID of the bank
		$bankId = null;
		
		$query = 'select id from rtp_bank where UPPER(BANK_DESCRIPTION) = :bank'; //  OTHER	
		$stid = oci_parse($conn, $query);
		
		oci_bind_by_name($stid, ":bank", $posted_data['Bank']);		
		oci_execute($stid);

		$count = 0;
		while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
			$bankId = $row['ID'];
			$count++;
		}
		
		if ($count == 0) {
			$query = 'select id from rtp_bank where UPPER(BANK_DESCRIPTION) = \'OTHER\''; //  OTHER	
			$stid = oci_parse($conn, $query);		
			oci_execute($stid);

			if (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
				$bankId = $row['ID'];
			}
		}

		oci_free_statement($stid);		
		
		// get the ID of the plan_id
		$planId = null;
		
		$query = 'select id from rtp_plan where UPPER(PLAN_DESCRIPTION) = UPPER(:plan)'; //  OTHER	
		$stid = oci_parse($conn, $query);
		
		oci_bind_by_name($stid, ":plan", $posted_data['Choice']);		
		oci_execute($stid);

		while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
			$planId = $row['ID'];
			$count++;
		}
		
		oci_free_statement($stid);				
		
		// get the ID of the 
		$companyId = null;
		$query = 'SELECT id FROM rtp_company WHERE (1=1)';
		
		$stid = oci_parse($conn, $query);
			
		oci_execute($stid);

		while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
			$companyId = $row['ID'];
			$count++;
		}

		oci_free_statement($stid);			
		
		$latitude = 0;
		$longitude = 0;
		
		$coords = getCoordinates($posted_data['Address']);
		
		// call member create procedure
		$sql = 'BEGIN   members_pkg.register_member_prc(
						p_company_id => :company_id,
						p_plan_id => :plan_id,
						p_first_name => :first_name,
						p_last_name => :last_name,
						p_id_number => :id_number,
						p_email_address => :email_address,
						p_user_name => :user_name,
						p_password => :password,
						p_address => :address,
						p_latitude => :latitude,
						p_longitude => :longitude,
						p_cellular_number => :cellular_number,
						p_manufacturer_id => :manufacturer_id,
						p_model => :model,
						p_registration => :registration,
						p_bank_id => :bank_id,
						p_branch_code => :branch_code,
						p_account_number => :account_number,
						p_notes => :notes,
						p_member_id => :member_id); 
				END;';

		$stmt = oci_parse($conn,$sql);
		$notes = 'Additional Family Members: ' . $posted_data['AdditionalFamilyMembersR60ppmonthTypetheirnamecellhereorleaveblank'] . '\nPassword: ' . $posted_data['Password'];

		//  Bind the input parameter$planId
		oci_bind_by_name($stmt,':company_id',$companyId);
		oci_bind_by_name($stmt,':plan_id',$planId);
		oci_bind_by_name($stmt,':first_name',$posted_data['Name']);
		oci_bind_by_name($stmt,':last_name',$posted_data['Surname']);
		oci_bind_by_name($stmt,':id_number',$posted_data['IDNumber']);
		oci_bind_by_name($stmt,':email_address',$posted_data['E-mail']);
		oci_bind_by_name($stmt,':user_name',$posted_data['Username']);
		oci_bind_by_name($stmt,':password',$posted_data['Password']);
		oci_bind_by_name($stmt,':address',$posted_data['Address']);
		oci_bind_by_name($stmt,':latitude',$latitude);
		oci_bind_by_name($stmt,':longitude',$longitude);
		oci_bind_by_name($stmt,':cellular_number',$posted_data['Mobile']);
		oci_bind_by_name($stmt,':manufacturer_id',$manufacturerId);
		oci_bind_by_name($stmt,':model',$posted_data['VehicleModel']);
		oci_bind_by_name($stmt,':registration',$posted_data['VehicleRegistrationnumber']);
		oci_bind_by_name($stmt,':bank_id',$bankId);
		oci_bind_by_name($stmt,':branch_code',$posted_data['BranchCode']);
		oci_bind_by_name($stmt,':account_number',$posted_data['AccountNumber']);
		oci_bind_by_name($stmt,':notes',$notes);

		// Bind the output parameter
		$memberId;
		oci_bind_by_name($stmt,':member_id',$memberId,32);

		oci_execute($stmt);		
		
		$r = oci_commit($conn);
		if (!$r) {
			$e = oci_error($conn);
			trigger_error(htmlentities($e['message']), E_USER_ERROR);
		}

		
		// Close the Oracle connection
		oci_close($conn);
    }
}; 

function getCoordinates($address){
	$address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern 
	$url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address"; 
	$response = @file_get_contents($url); 
	$json = json_decode($response,TRUE); //generate array object from the response from the web 
	$array = array(
		"lat" => $json['results'][0]['geometry']['location']['lat'],
		"lng" => $json['results'][0]['geometry']['location']['lng']
	);	
	return ($array); 
}

//add_action('init', create_function('',
//    'add_action("wpcf7_before_send_mail", "action_wpcf7_before_send_mail");'));

// define the wpcf7_validate callback 
function filter_wpcf7_validate( $result, $tags ) { 
    // make filter magic happen here... 
	// error_log(print_r($_POST, TRUE));
	//error_log(print_r($result, TRUE));
	// error_log(print_r($result->get_invalid_fields(), TRUE)); // this break
	//error_log(count($result[0]));
	//error_log(print_r($tags, TRUE));
	//$posted_data = $submission->get_posted_data();
	//first check other validation results, if none then process
	if ( empty( $result->get_invalid_fields() ) ) { 
	//if ($result['valid'] == true) {
		$posted_data = $_POST;
		error_log($posted_data['_wpcf7']);
		if ($posted_data['_wpcf7'] == 300) { // SAB 	
			error_log("SAB");			
			try {
				// check for CRM 
				if ($posted_data['Choice'] == 'Choose Business Unit') throw new Exception('Please choose a Business Unit');	
				
				class Member {
					public $firstName = "";
					public $lastName  = "";
					public $idNumber = "";
					public $emailAddress = "";
					public $manufacturer = "";
					public $mobileNumber = "";
					public $vehicleModel = "";
					public $vehicleRegistration = "";
					public $address = "";
					public $corporateNumber = "";
					public $notes = "";
					public $mainMemberNumber = "12863";
				}						
				
				$m = new Member();
				$m->firstName = $posted_data['Name'];
				$m->lastName = $posted_data['Surname'];
				$m->idNumber = $posted_data['IDNumber'];
				$m->emailAddress = $posted_data['E-mail'];
				$m->manufacturer = $posted_data['VehicleMake'];
				//$mobileNumber = $posted_data['Mobile'];
				//$mobileNumber = preg_replace('/\s+/', '', $mobileNumber);
				//$mobileNumber = strlen($mobileNumber) <= 10 ? "+027" + substr($mobileNumber, 1) : $mobileNumber;
				$m->mobileNumber = "+027" . substr($posted_data['Mobile'], 1);
				$m->vehicleModel = $posted_data['VehicleModel'];
				$m->vehicleRegistration = $posted_data['VehicleRegistrationnumber'];
				$m->address = $posted_data['Address'];
				$m->corporateNumber = $posted_data['EmployeeNumber'];
				$m->notes = 'Username: ' . $posted_data['Username'] . '\n' .
					// 'Password: ' . $posted_data['Password'] . '\n' .
					'Business Unit: ' . $posted_data['Choice'];
				//$m->mainMemberNumber = '8774'; // Roadtrip Adhoc
				$m->mainMemberNumber = '12863'; // SAB
				
				$data_string = json_encode($m);  
				
				$ch = curl_init('https://196.13.254.86:8181/ords/roadtrip/members/corporate');  				
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // change certificate to 41.169.2.10
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_SSLVERSION, 1);					                                                                    
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
					'Content-Type: application/json',                                                                                
					'Content-Length: ' . strlen($data_string))                                                                       
				);                                                                                                                   
						
				$response = curl_exec($ch);						
				
				if (!empty($response)) {
					error_log(print_r($response, TRUE));
					throw new Exception($response);
					//throw(print_r($response));
				} else if (curl_error($ch)) {
					error_log(curl_error($ch));
					throw new Exception(curl_error($ch));
				}
				
				return $result;
			} catch (Exception $e) {
				$result['valid'] = false;
				$result['reason'] = array( $name => $e->getMessage() );
				return $result;
			}
		}
		else if ($posted_data['_wpcf7'] == 252) { // discovery
			error_log("Discovery");
			try {			
				class Member {
					public $firstName = "";
					public $lastName  = "";
					public $emailAddress = "";
					public $manufacturer = "";					
					public $vehicleModel = "";
					public $vehicleColour = "";
					public $vehicleRegistration = "";
					public $address = "";
					public $mobileNumber = "";
					public $idNumber = "";
					public $discoveryNumber = "";
					public $cardHolderName = "";
					public $creditCardNumber = "";
					public $creditCardExpiryDate = "";
					public $creditCardCVV = "";
					public $notes = "";
					public $memberLevel = "DISCOVERY";
					public $plan = "Discovery PAYG";
				}				
				
				$m = new Member();
				$m->firstName = $posted_data['Name'];
				$m->lastName = $posted_data['Surname'];
				$m->emailAddress = $posted_data['E-mail'];
				$m->discoveryNumber = $posted_data['DiscoveryInsureRewardCardNumber'];
				$m->manufacturer = $posted_data['VehicleMake'];
				$m->vehicleModel = $posted_data['VehicleModel'];
				$m->vehicleColour = $posted_data['VehicleColour'];
				$m->vehicleRegistration = $posted_data['VehicleRegistrationnumber'];				
				$m->address = $posted_data['Address'];
				//$mobileNumber = $posted_data['Mobile'];
				//$mobileNumber = preg_replace('/\s+/', '', $mobileNumber);
				//$mobileNumber = strlen($mobileNumber) <= 10 ? "+027" + substr($mobileNumber, 1) : $mobileNumber;
				$m->mobileNumber = "+027" . substr($posted_data['Mobile'], 1);
				//$m->mobileNumber = $mobileNumber;
				$m->idNumber = $posted_data['IDNumber'];
				$m->memberLevel = "DISCOVERY";
				$m->plan = 'Discovery PAYG';
				
				// credit card
				$m->cardHolderName = $posted_data['CardHolderName'];
				$m->creditCardNumber = $posted_data['CreditCardNumber'];				
				//$m->$creditCardMonth = $posted_data['CreditCardMonth'];
				//$m->$creditCardYear = $posted_data['CreditCardYear'];
				$m->creditCardExpiryDate = str_pad($posted_data['CreditCardMonth'], 2, "0", STR_PAD_LEFT) . $posted_data['CreditCardYear'];
				$m->creditCardCVV = $posted_data['CreditCardCVV'];
					
				$m->notes = 'Username: ' . $posted_data['Username'] . '\n' .
					// 'Password: ' . $posted_data['Password'];
				
				$data_string = json_encode($m);  
				error_log(print_r($data_string, TRUE));			
				$ch = curl_init('https://196.13.254.86:8181/ords/roadtrip/members/register');                                                                      
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // change certificate to 41.169.2.10
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_SSLVERSION, 1);				
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
					'Content-Type: application/json',                                                                                
					'Content-Length: ' . strlen($data_string))                                                                       
				);                                                                                                                   
						
				$response = curl_exec($ch);						
				error_log(print_r($response, TRUE));
				if (!empty($response)) {
					error_log(print_r($response, TRUE));
					throw new Exception($response);
					//throw(print_r($response));
				} else if (curl_error($ch)) {
					error_log(curl_error($ch));
					throw new Exception(curl_error($ch));
				}

				return $result;
			} catch (Exception $e) {
				error_log($e->getMessage());
				$result['valid'] = false;
				$result['reason'] = array( $name => $e->getMessage() );
				return $result;
			}			
		}
		else if ($posted_data['_wpcf7'] == 374) { // Pernod
			error_log("Pernod");
			try {
				// check for CRM 
				if ($posted_data['Choice'] == 'Choose Business Unit') throw new Exception('Please choose a Business Unit');	
				
				class Member {
					public $firstName = "";
					public $lastName  = "";
					public $idNumber = "";
					public $emailAddress = "";
					public $manufacturer = "";
					public $mobileNumber = "";
					public $vehicleModel = "";
					public $vehicleRegistration = "";
					public $address = "";
					public $corporateNumber = "";
					public $notes = "";
					public $mainMemberNumber = "8158";
				}						
				
				$m = new Member();
				$m->firstName = $posted_data['Name'];
				$m->lastName = $posted_data['Surname'];
				$m->idNumber = $posted_data['IDNumber'];
				$m->emailAddress = $posted_data['E-mail'];
				$m->manufacturer = $posted_data['VehicleMake'];
				//$mobileNumber = $posted_data['Mobile'];
				//$mobileNumber = preg_replace('/\s+/', '', $mobileNumber);
				//$mobileNumber = strlen($mobileNumber) <= 10 ? "+027" + substr($mobileNumber, 1) : $mobileNumber;
				$m->mobileNumber = "+027" . substr($posted_data['Mobile'], 1);
				$m->vehicleModel = $posted_data['VehicleModel'];
				$m->vehicleRegistration = $posted_data['VehicleRegistrationnumber'];
				$m->address = $posted_data['Address'];
				$m->corporateNumber = $posted_data['EmployeeNumber'];
				$m->notes = 'Username: ' . $posted_data['Username'] . '\n' .
					// 'Password: ' . $posted_data['Password'] . '\n' .
					'Business Unit: ' . $posted_data['Choice'];
				$m->mainMemberNumber = '8158'; // Pernod
				
				$data_string = json_encode($m);  
				
				$ch = curl_init('https://196.13.254.86:8181/ords/roadtrip/members/corporate');                                                                      
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // change certificate to 41.169.2.10
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_SSLVERSION, 1);					                                                                    
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
					'Content-Type: application/json',                                                                                
					'Content-Length: ' . strlen($data_string))                                                                       
				);                                                                                                                   
						
				$response = curl_exec($ch);						
				
				if (!empty($response)) {
					error_log(print_r($response, TRUE));
					throw new Exception($response);
					//throw(print_r($response));
				} else if (curl_error($ch)) {
					error_log(curl_error($ch));
					throw new Exception(curl_error($ch));
				}
				
				return $result;
			} catch (Exception $e) {
				$result['valid'] = false;
				$result['reason'] = array( $name => $e->getMessage() );
				return $result;
			}			
		}		
		else if ($posted_data['_wpcf7'] == 45) { // signup
			error_log("Normal Member");
			try {			
				class Member {
					public $firstName = "";
					public $lastName  = "";
					public $emailAddress = "";
					public $manufacturer = "";					
					public $vehicleModel = "";
					public $vehicleColour = "";
					public $vehicleRegistration = "";
					public $address = "";
					public $mobileNumber = "";
					public $idNumber = "";
					//public $discoveryNumber = "";
					//public $cardHolderName = "";
					//public $creditCardNumber = "";
					//public $creditCardExpiryDate = "";
					//public $creditCardCVV = "";
					public $memberLevel = "";
					public $bank = "";
					public $branchCode = "";
					public $accountNumber = "";
					public $notes = "";
				}				
				
				/*$streamContext = stream_context_create(array(
					'http'=>array(
						'user_agent' => 'PHPSoapClient'
					),				
					'ssl' => array(
						//'ciphers' => 'RC4-SHA',
						'verify_peer' => false,
						'verify_peer_name' => false,
						'allow_self_signed' => true
					)
				));				

				// SOAP 1.2 client
				$params = array(
					'cache_wsdl' => WSDL_CACHE_NONE,
					'encoding' => 'UTF-8',
					'verifypeer' => false,
					'verifyhost' => false,
					'soap_version' => SOAP_1_2,
					'trace' => 1,
					'exceptions' => 1,
					'connection_timeout' => 180,
					'stream_context' => $streamContext
				);				

				
				// check ID number
				// Create the SOAP Client and connect to the netserv file
				$soap = new SoapClient("https://www.roadtrip.co.za/netcash.webservices.validation.xml?wsdl", $params);
				// This is an object of the parameters we are passing.
				class SOAPIDNumberRequest {
					var $Username;
					var $Password;
					var $Pin;
					var $IDNumber;
				}
				// Create and populate the request
				$request = new SOAPIDNumberRequest();
				$request->Username = "ROA003WEB";
				$request->Password = "156797";
				$request->Pin = "984711";
				$request->IDNumber = $posted_data['IDNumber'];
				// Send the request
				$result = $soap->CDVCheckIDNumber($request);				
				throw new Exception($result);
				*/
				
				// check bank details				
				
				$m = new Member();
				$m->firstName = $posted_data['Name'];
				$m->lastName = $posted_data['Surname'];
				$m->emailAddress = $posted_data['E-mail'];
				//$m->discoveryNumber = $posted_data['DiscoveryInsureRewardCardNumber'];
				$m->manufacturer = $posted_data['VehicleMake'];
				$m->vehicleModel = $posted_data['VehicleModel'];
				$m->vehicleColour = $posted_data['VehicleColour'];
				$m->vehicleRegistration = $posted_data['VehicleRegistrationnumber'];				
				$m->address = $posted_data['Address'];
				//amend to +27
				//$mobileNumber = $posted_data['Mobile'];
				//$mobileNumber = preg_replace('/\s+/', '', $mobileNumber);
				//$mobileNumber = strlen($mobileNumber) <= 10 ? "+027" + substr($mobileNumber, 1) : $mobileNumber;
				//$m->mobileNumber = $mobileNumber;
				$m->mobileNumber = "+027" . substr($posted_data['Mobile'], 1); 
				$m->idNumber = $posted_data['IDNumber'];
				$m->memberLevel = 'NORMAL';
				$m->bank = $posted_data['Bank'];
				$m->branchCode = $posted_data['BranchCode'];
				$m->accountNumber = $posted_data['AccountNumber'];
				$m->plan = $posted_data['Choice'];
				$m->sendRegistration = '1';
				
				// credit card
				//$m->cardHolderName = $posted_data['CardHolderName'];
				//$m->creditCardNumber = $posted_data['CreditCardNumber'];				
				//$m->creditCardExpiryDate = str_pad($posted_data['CreditCardMonth'], 2, "0", STR_PAD_LEFT) . $posted_data['CreditCardYear'];
				//$m->creditCardCVV = $posted_data['CreditCardCVV'];
					
				$m->notes = 'Additional Family Members: ' . $posted_data['AdditionalFamilyMembersR60ppmonthTypetheirnamecellhereorleaveblank'] . 
					'\r\nUser Name: ' . $posted_data['Username'] .
					// '\r\nPassword: ' . $posted_data['Password'] .
					'\r\nBank: ' . $posted_data['Bank'] .
					'\r\nBranch Code: ' . $posted_data['BranchCode'] .
					'\r\nAccount Number: ' . $posted_data['AccountNumber'];					
				
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.roadtrip.co.za/v1/authenticate",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => false,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>"{\n    \"timgalza\": \"myusername\",\n    \"password\": \"Pa55w0rd1\"\n}",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json"
  ),
));

$auth_resp = curl_exec($curl);
$err = curl_error($curl);
				
if ($err) {
	error_log(curl_error($ch));
	throw new Exception(curl_error($ch));
} 

curl_close($curl);	

				$obj = json_decode($auth_resp);
				$data_string = json_encode($m);
				
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.roadtrip.co.za/v1/members/register",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => false,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => $data_string,
  CURLOPT_HTTPHEADER => array(
    "Authorization: Bearer " . $obj->{'token'},
    "Content-Type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	error_log(curl_error($ch));
	throw new Exception(curl_error($ch));;
} 

return result;
				
				/*$data_string = json_encode($m);  
				error_log(print_r($data_string, TRUE));			
				//$ch = curl_init('https://196.13.254.86:8181/ords/roadtrip/members/register'); 
				$ch = curl_init('https://api.roadtrip.co.za/v1/authenticate');
				
				$auth_str = '{
						"username": "timgaulza",
						"password": "Pa55w0rd1"
				}';
				
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
					'Content-Type: application/json',                                                                                
					'Content-Length: ' . strlen($auth_str))                                                                       
				); 
				curl_setopt($ch, CURLOPT_POSTFIELDS, $auth_str); 
				$auth_resp = curl_exec($ch);
				error_log(print_r($auth_resp, FALSE));
				$obj = json_decode($auth_resp);
				error_log(print_r($obj, TRUE));
error_log('Authorization: Bearer ' . $obj->{'token'});
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // change certificate to 41.169.2.10
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_SSLVERSION, 1);				
				curl_setopt($ch, CURLOPT_AUTOREFERER, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      

				$ch = curl_init('https://api.roadtrip.co.za/v1/members/register');                                                                      
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
				//curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
					'Content-Type: application/json',                                                                                
					'Content-Length: ' . strlen($data_string)),
					'Authorization: Bearer ' . $obj->{'token'}
				);                                                                                                                   
						
				$response = curl_exec($ch);						
				error_log(print_r($response, TRUE));
				if (!empty($response)) {
					error_log(print_r($response, TRUE));
					throw new Exception($response);
					//throw(print_r($response));
				} else if (curl_error($ch)) {
					error_log(curl_error($ch));
					throw new Exception(curl_error($ch));
				}

				return $result;*/
			} catch (Exception $e) {
				error_log($e->getMessage());
				$result['valid'] = false;
				$result['reason'] = array( $name => $e->getMessage() );
				return $result;
			}		
			/*try {

				$conn = oci_connect("roadtrip", "Pa55w0rd1", "//41.169.2.10/xe");
				//$conn = oci_connect("roadtrip", "Pa55w0rd1", "//www.roadtrip.co.za/xe");
				if (!$conn) {
				   $m = oci_error();
				   error_log(print_r($m['message'], TRUE));
				}
				else {
				   error_log(print_r("Connected to Oracle!", TRUE));
				}
				
				// get the ID of the manufacturer
				$manufacturerId = null;
				
				$query = 'select id from rtp_manufacturer where UPPER(MANUFACTURER_DESCRIPTION) = :manufacturer'; //  OTHER	
				$stid = oci_parse($conn, $query);
				
				oci_bind_by_name($stid, ":manufacturer", $posted_data['VehicleMake']);		
				oci_execute($stid);

				$count = 0;
				while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
					$manufacturerId = $row['ID'];
					$count++;
				}
				
				if ($count == 0) {
					$query = 'select id from rtp_manufacturer where UPPER(MANUFACTURER_DESCRIPTION) = \'OTHER\''; //  OTHER	
					$stid = oci_parse($conn, $query);		
					oci_execute($stid);

					if (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
						$manufacturerId = $row['ID'];
					}
				}

				oci_free_statement($stid);
				
				// get the ID of the bank
				$bankId = null;
				
				$query = 'select id from rtp_bank where UPPER(BANK_DESCRIPTION) = :bank'; //  OTHER	
				$stid = oci_parse($conn, $query);
				
				oci_bind_by_name($stid, ":bank", $posted_data['Bank']);		
				oci_execute($stid);

				$count = 0;
				while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
					$bankId = $row['ID'];
					$count++;
				}
				
				if ($count == 0) {
					$query = 'select id from rtp_bank where UPPER(BANK_DESCRIPTION) = \'OTHER\''; //  OTHER	
					$stid = oci_parse($conn, $query);		
					oci_execute($stid);

					if (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
						$bankId = $row['ID'];
					}
				}

				oci_free_statement($stid);		
				
				// get the ID of the plan_id
				$planId = null;
				
				$query = 'select id from rtp_plan where UPPER(PLAN_DESCRIPTION) = UPPER(:plan)'; //  OTHER	
				$stid = oci_parse($conn, $query);
				
				oci_bind_by_name($stid, ":plan", $posted_data['Choice']);		
				oci_execute($stid);

				while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
					$planId = $row['ID'];
					$count++;
				}
				
				error_log('plan ' . $posted_data['Choice']);
				error_log('planId ' . $planId);

				oci_free_statement($stid);				
				
				// get the ID of the 
				$companyId = null;
				$query = 'SELECT id FROM rtp_company WHERE (1=1)';
				
				$stid = oci_parse($conn, $query);
					
				oci_execute($stid);

				while (($row = oci_fetch_array($stid, OCI_BOTH)) != false) {
					$companyId = $row['ID'];
					$count++;
				}

				oci_free_statement($stid);			
				
				$latitude = 0;
				$longitude = 0;
				
				$coords = getCoordinates($posted_data['Address']);
				error_log($coords['lat']);
				error_log($coords['lng']);
				
				// call member create procedure
				$sql = 'BEGIN   members_pkg.register_member_prc(
								p_company_id => :company_id,
								p_plan_id => :plan_id,
								p_first_name => :first_name,
								p_last_name => :last_name,
								p_id_number => :id_number,
								p_email_address => :email_address,
								p_address => :address,
								p_latitude => :latitude,
								p_longitude => :longitude,
								p_cellular_number => :cellular_number,
								p_manufacturer_id => :manufacturer_id,
								p_model => :model,
								p_registration => :registration,
								p_bank_id => :bank_id,
								p_branch_code => :branch_code,
								p_account_number => :account_number,
								p_notes => :notes,
								p_member_id => :member_id); 
						END;';

				$stmt = oci_parse($conn,$sql);
				$notes = 'Additional Family Members: ' . $posted_data['AdditionalFamilyMembersR60ppmonthTypetheirnamecellhereorleaveblank'] . 
					'\r\nUser Name: ' . $posted_data['Username'] .
					'\r\nPassword: ' . $posted_data['Password'] .
					'\r\nBank: ' . $posted_data['Bank'] .
					'\r\nBranch Code: ' . $posted_data['BranchCode'] .
					'\r\nAccount Number: ' . $posted_data['AccountNumber'];

				//  Bind the input parameter$planId
				oci_bind_by_name($stmt,':company_id',$companyId);
				oci_bind_by_name($stmt,':plan_id',$planId);
				oci_bind_by_name($stmt,':first_name',$posted_data['Name']);
				oci_bind_by_name($stmt,':last_name',$posted_data['Surname']);
				oci_bind_by_name($stmt,':id_number',$posted_data['IDNumber']);
				oci_bind_by_name($stmt,':email_address',$posted_data['E-mail']);
				oci_bind_by_name($stmt,':address',$posted_data['Address']);
				oci_bind_by_name($stmt,':latitude',$latitude);
				oci_bind_by_name($stmt,':longitude',$longitude);
				oci_bind_by_name($stmt,':cellular_number',$posted_data['Mobile']);
				oci_bind_by_name($stmt,':manufacturer_id',$manufacturerId);
				oci_bind_by_name($stmt,':model',$posted_data['VehicleModel']);
				oci_bind_by_name($stmt,':registration',$posted_data['VehicleRegistrationnumber']);
				oci_bind_by_name($stmt,':bank_id',$bankId);
				oci_bind_by_name($stmt,':branch_code',$posted_data['BranchCode']);
				oci_bind_by_name($stmt,':account_number',$posted_data['AccountNumber']);
				oci_bind_by_name($stmt,':notes',$notes);
				//array_walk($posted_data, create_function('&$i,$k','$i=" $k=\"$i\"";'));
				//oci_bind_by_name($stmt,':notes',implode("\n",$arr));			
				
				// Bind the output parameter
				$memberId;
				oci_bind_by_name($stmt,':member_id',$memberId,32);
				
				$r = @oci_execute($stmt);	
				if (!$r) {
					$e = oci_error($stmt);
					error_log('oci_execute: error');
					error_log($e['message']);
					$result['valid'] = false;
					$result['reason'] = array( $name => $e['message'] );
					oci_close($conn);
					return $result; 
				}		
				
				$r = oci_commit($conn);
				if (!$r) {
					$e = oci_error($conn);
					//trigger_error(htmlentities($e['message']), E_USER_ERROR);
					error_log('oci_commit: error');
					error_log($e['message']);
					$result['valid'] = false;
					$result['reason'] = array( $name => $e['message'] );
					oci_close($conn);
					return $result; 
				}

				
				// Close the Oracle connection
				oci_close($conn);
			} catch (Exception $e) {
				error_log('invalid');
				$result['valid'] = false;
				$result['reason'] = array( $name => $e->getMessage() );
				//return $result; 
			}	*/			
		} // if
		else if ($posted_data['_wpcf7'] == 425) { // Complaints
			error_log("Complaints");
			try {			
				class Complaint {
					public $fullName = "";
					public $contactNumber = "";
					public $emailAddress = "";
					public $city = "";					
					public $complaintDriver = "";
					public $complaintCallCentre = "";
					public $collectionDate = "";
					public $collectionTime = "";
					public $complaintDetails = "";
				}				

				// check bank details								
				$c = new Complaint();
				$c->fullName = $posted_data['name'];
				$c->contactNumber = $posted_data['tel-52'];
				$c->emailAddress = $posted_data['email-973'];
				$c->city = $posted_data['city'];
				$c->complaintDriver = $posted_data['complaint_driver'];
				$c->complaintCallCentre = $posted_data['complaint_call_centre'];
				$c->collectionDate = $posted_data['DateofCollection'];
				$c->collectionTime = $posted_data['time-5'];
				$c->complaintDetails = $posted_data['Complaintdetails'];
								
				$data_string = json_encode($c);  
				error_log(print_r($data_string, TRUE));			
				$ch = curl_init('https://196.13.254.86:8181/ords/roadtrip/members/complaint');                                                                      
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // change certificate to 41.169.2.10
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_SSLVERSION, 1);				
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
				curl_setopt($ch, CURLOPT_AUTOREFERER, true);
				//curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
					'Content-Type: application/json',                                                                                
					'Content-Length: ' . strlen($data_string))                                                                       
				);                                                                                                                   
						
				$response = curl_exec($ch);						
				error_log(print_r($response, TRUE));
				if (!empty($response)) {
					error_log(print_r($response, TRUE));
					throw new Exception($response);
					//throw(print_r($response));
				} else if (curl_error($ch)) {
					error_log(curl_error($ch));
					throw new Exception(curl_error($ch));
				}

				return $result;
			} catch (Exception $e) {
				error_log($e->getMessage());
				$result['valid'] = false;
				$result['reason'] = array( $name => $e->getMessage() );
				return $result;
			}		
		}
		else if ($posted_data['_wpcf7'] == 2451) { // Lions 	
			error_log("Lions");			
			try {
				// check for CRM 
				// if ($posted_data['Choice'] == 'Choose Business Unit') throw new Exception('Please choose a Business Unit');	
				
				class Member {
					public $firstName = "";
					public $lastName  = "";
					// public $idNumber = "";
					public $emailAddress = "";
					public $manufacturer = "";
					public $mobileNumber = "";
					public $vehicleModel = "";
					public $vehicleRegistration = "";
					public $address = "";
					public $corporateNumber = "";
					public $notes = "";
					public $mainMemberNumber = '15716'; // Lions Player/Playing Staff
				}						
				
				$m = new Member();
				$m->firstName = $posted_data['Name'];
				$m->lastName = $posted_data['Surname'];
				$m->idNumber = $posted_data['IDNumber'];
				$m->emailAddress = $posted_data['E-mail'];
				$m->manufacturer = $posted_data['VehicleMake'];
				//$mobileNumber = $posted_data['Mobile'];
				//$mobileNumber = preg_replace('/\s+/', '', $mobileNumber);
				//$mobileNumber = strlen($mobileNumber) <= 10 ? "+027" + substr($mobileNumber, 1) : $mobileNumber;
				$m->mobileNumber = "+027" . substr($posted_data['Mobile'], 1);
				$m->vehicleModel = $posted_data['VehicleModel'];
				$m->vehicleRegistration = $posted_data['VehicleRegistrationnumber'];
				$m->address = $posted_data['Address'];
				$m->corporateNumber = $posted_data['EmployeeNumber'];
				$m->notes = 'Username: ' . $posted_data['Username'] . '\n' .
					// 'Password: ' . $posted_data['Password'] . '\n' .
					'Type of Member: ' . $posted_data['Choice'];
				// $m->mainMemberNumber = '12863'; // SAB
				if ($posted_data['Choice'] == 'Lions Player/Playing Staff') {
					$m->mainMemberNumber = '15716'; // Lions Player/Playing Staff
				} else {
					$m->mainMemberNumber = '15717'; // Lions Staff				
				}
				
				$data_string = json_encode($m);  
				
				$ch = curl_init('https://196.13.254.86:8181/ords/roadtrip/members/corporate');  				
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // change certificate to 41.169.2.10
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_SSLVERSION, 1);					                                                                    
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
					'Content-Type: application/json',                                                                                
					'Content-Length: ' . strlen($data_string))                                                                       
				);                                                                                                                   
						
				$response = curl_exec($ch);						
				
				if (!empty($response)) {
					error_log(print_r($response, TRUE));
					throw new Exception($response);
					//throw(print_r($response));
				} else if (curl_error($ch)) {
					error_log(curl_error($ch));
					throw new Exception(curl_error($ch));
				}
				
				return $result;
			} catch (Exception $e) {
				$result['valid'] = false;
				$result['reason'] = array( $name => $e->getMessage() );
				return $result;
			}
		}	
	} // if empty($result)
	return $result;
}; 



add_filter( 'wpcf7_validate', 'filter_wpcf7_validate', 10, 2 ); 